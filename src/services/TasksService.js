class TasksService {
  /**
   * @param {Object} $http
   * @param {Object} localStorageService
   * @returns {TasksService}
   */
  constructor($http, localStorageService) {
    this.localStorageService = localStorageService;
    this.$http = $http;
    this.tasks = [];
  }

  /**
   * @param {number|string} id
   * @returns {Promise}
   */
  getById(id) {
    return this.fetch().then(tt => tt.find(t => t._id.toString() === id));
  }

  /**
   * @param {Object} item
   * @returns {Promise}
   */
  addOne(item) {
    const service = this;
    const localItem = angular.copy(item);
    const threshold = 24;

    return this.fetch().then((tasks) => {
      const maxId = TasksService.maxId(tasks) || 0;
      const durationLimitErr = new Error('Task cannot be saved due to total duration limit.');

      let tasksByTheDate = [];
      let taskAlreadyIn = false;
      let totalTimeByDate = 0;
      let oldDuration = 0;

      if (!localItem.hasOwnProperty('_id')) {
        localItem._id = maxId + 1;
      }

      if (typeof localItem.date !== 'string') {
        localItem.date = new Date(localItem.date.setHours(0, 0, 0, 0)).toISOString();
      }

      tasksByTheDate = tasks.filter(t => t.date === localItem.date) || [];
      totalTimeByDate = tasksByTheDate.map(t => t.duration).reduce((p, n) => p + n, 0);
      taskAlreadyIn = tasksByTheDate.find(t => t._id === localItem._id);

      if (taskAlreadyIn) {
        oldDuration = taskAlreadyIn.duration;
      }

      if (threshold - totalTimeByDate + oldDuration >= localItem.duration) {
        service.tasks = TasksService.mergeById(tasks, [localItem]);
        service.localStorageService.set('tasks', service.tasks);

        return Promise.resolve(localItem);
      }

      return Promise.reject(durationLimitErr);
    });
  }

  /**
   * @returns {Promise}
   */
  fetch() {
    return this.$http.get('/data/tasks.json').then(r => {
      const stored = this.localStorageService.get('tasks') || [];
      const merged = TasksService.mergeById(r.data, stored);

      this.localStorageService.set('tasks', merged);
      this.tasks = merged;

      return this.tasks;
    });
  }

  /**
   * @param {Array} tasks
   * @returns {number}
   */
  static maxId(tasks) {
    return Math.max.apply(Math, (tasks || []).map(t => t._id));
  }

  /**
   * @param {Array} arr1
   * @param {Array} arr2
   * @returns {Array} - CHanged arr1
     */
  static mergeById(arr1, arr2) {
    angular.forEach(arr2, arr2obj => {
      const arr1obj = arr1.find(arr1objInner => arr1objInner._id === arr2obj._id);

      if (arr1obj) {
        angular.extend(arr1obj, arr2obj);
      } else {
        arr1.push(arr2obj);
      }
    });

    return arr1;
  }
}

angular
  .module('TodoApp')
  .service('TasksService', TasksService);
