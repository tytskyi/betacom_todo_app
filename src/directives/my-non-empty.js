/**
 * @returns {Object} - Directive options
 */
const myNonEmpty = () => ({
  restrict: 'A',
  require: 'ngModel',
  /**
   * @param {Object} scope
   * @param {Object} element
   * @param {Object} attributes
   * @param {Object} ngModel
   * @returns {undefined}
   */
  link: (scope, element, attributes, ngModel) => {
    ngModel.$validators['my-non-empty'] = (modelValue) => !!(modelValue || '').trim();
  },
});

angular
  .module('TodoApp')
  .directive('myNonEmpty', myNonEmpty);
