angular.module('TodoApp', [
  'templates',
  'ui.router',
  'ngMaterial',
  'ngMessages',
  'md.data.table',
  'LocalStorageModule',
]);
