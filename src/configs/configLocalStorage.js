/**
 * @param {Object} localStorageServiceProvider
 * @returns {undefined}
 */
const configLocalStorage = (localStorageServiceProvider) => {
  localStorageServiceProvider.setPrefix('todoTasks');
};

angular
  .module('TodoApp')
  .config(configLocalStorage);
