const leftPad = (num) => (num < 10 ? `0${num}` : num);

/**
 * @param {Object} $mdDateLocaleProvider
 * @returns {undefined}
 */
const configMdDatepicker = ($mdDateLocaleProvider) => {
  $mdDateLocaleProvider.formatDate = (date) => {
    const d = date;
    const day = leftPad(d.getDate());
    const month = leftPad(d.getMonth() + 1);
    const year = leftPad(d.getFullYear());

    return `${day}.${month}.${year}`;
  };
};

angular
  .module('TodoApp')
  .config(configMdDatepicker);
