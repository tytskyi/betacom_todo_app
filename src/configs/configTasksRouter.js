/**
 * @param {string} title
 * @returns {Function}
 */
const setPageTitle = title => {
  /**
   * @param {Object} $rootScope
   * @returns {undefined}
   */
  const inner = $rootScope => { $rootScope.pageTitle = title; };
  inner.$inject = ['$rootScope'];
  return inner;
};

/**
 * @param {Object} $stateProvider
 * @param {Object} $urlRouterProvider
 * @returns {undefined}
 */
const configTasksRouter = ($stateProvider, $urlRouterProvider) => {
  $urlRouterProvider.otherwise('/tasks');

  $stateProvider
    .state('taskList', {
      abstract: true,
      url: '/tasks',
      templateUrl: 'src/templates/tasks.html',
    })
    .state('taskList.show', {
      onEnter: setPageTitle('Tasks listing'),
      url: '',
      templateUrl: 'src/templates/tasks.show.html',
      controller: 'TasksListCtrl as tasksList',
      resolve: {
        loadedTasks: TasksService => TasksService.fetch(),
      },
    })
    .state('taskList.add', {
      onEnter: setPageTitle('Add task'),
      url: '/add',
      templateUrl: 'src/templates/tasks.add.html',
      controller: 'TaskCtrl as task',
      resolve: {
        // NOTE: hack to make single controller work in differents modes.
        loadedTask: () => undefined,
      },
    })
    .state('taskList.edit', {
      onEnter: setPageTitle('Edit task'),
      url: '/edit/:id',
      templateUrl: 'src/templates/tasks.add.html',
      controller: 'TaskCtrl as task',
      resolve: {
        loadedTask: (TasksService, $stateParams) => TasksService.getById($stateParams.id),
      },
    });
};

angular
  .module('TodoApp')
  .config(configTasksRouter);
