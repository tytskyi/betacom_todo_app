/**
 * @param {Object} $mdThemingProvider
 * @returns {undefined}
 */
const configMaterialTheme = ($mdThemingProvider) => {
  $mdThemingProvider.theme('default').primaryPalette('indigo');
};

angular
  .module('TodoApp')
  .config(configMaterialTheme);
