class TaskCtrl {
  /**
   * @param {Array|undefined} loadedTask
   * @param {Object} $state
   * @param {Object} $scope
   * @param {Object} TasksService
   * @returns {TaskCtrl}
   */
  constructor(loadedTask, $state, $scope, TasksService) {
    const masterTask = {
      date: new Date(),
      duration: 1,
      name: '',
      done: false,
      description: '',
    };

    this.$state = $state;
    this.$scope = $scope;
    this.TasksService = TasksService;

    this.editingMode = !!loadedTask;
    this.pendingTask = angular.copy(loadedTask || masterTask);
    this.pendingTask.date = new Date(this.pendingTask.date);

    $scope.$watch(
        () => this.pendingTask.date,
        () => this.$scope.taskForm.duration.$setValidity('duration-limit', true)
      );
  }

  save(event) {
    const vm = this;

    this.TasksService.addOne(this.pendingTask).then(() => {
      vm.$scope.taskForm.duration.$setValidity('duration-limit', true);
      vm.$state.go('^.show');
    }, () => {
      vm.$scope.taskForm.duration.$setValidity('duration-limit', false);
    });

    event.preventDefault();
  }
}

angular
  .module('TodoApp')
  .controller('TaskCtrl', TaskCtrl);
