class TasksListCtrl {
  /**
   * @param {Array} loadedTasks
   * @param {Object} TasksService
   * @returns {TasksListCtrl}
   */
  constructor(loadedTasks, TasksService) {
    this.TasksService = TasksService;
    this.tasks = TasksListCtrl.groupByDate(loadedTasks);
    this.orderOption = 'name';
  }

  change(task) {
    this.TasksService.addOne(task);
  }

  /**
   *
   * @param {Array} tasks
   * @returns {Array}
     */
  static groupByDate(tasks) {
    let dates = tasks.map(t => t.date);
    dates = Array.from(new Set(dates));
    dates = dates.sort((p, n) => new Date(n).getTime() - new Date(p).getTime());

    const tasksByDate = dates.map(d => ({
      date: d,
      tasks: tasks.filter(t => t.date === d),
    }));

    return tasksByDate;
  }
}

angular
  .module('TodoApp')
  .controller('TasksListCtrl', TasksListCtrl);
