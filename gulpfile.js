var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var ngAnnotate = require('gulp-ng-annotate');
var browserSync = require('browser-sync').create();
var wrap = require('gulp-wrap');
var babel = require('gulp-babel');
var eslint = require('gulp-eslint');
var angularTemplateCache = require('gulp-angular-templatecache');
var addStream = require('add-stream');

var JS_SOURCES = ['src/modules/templates.js', 'src/modules/*.js', 'src/**/*.js'];
var TPL_SOURCES = ['src/**/*.html'];
var BABEL_OPTIONS = {presets: ['es2015']};
var ESLINT_OPTIONS = {
    extends: 'airbnb',
    rules: {
        'no-underscore-dangle': 'off',
        'no-param-reassign': ['error', {'props': false}]
    },
    globals: {angular: true},
    envs: ['browser']
};

function prepareTemplates() {
    return gulp.src(TPL_SOURCES)
        .pipe(angularTemplateCache({
            transformUrl: function(url) {
                return 'src/' + url;
            }
        }));
}

gulp.task('js', function () {
    return gulp.src(JS_SOURCES)
        .pipe(sourcemaps.init())
        .pipe(eslint(ESLINT_OPTIONS))
        .pipe(eslint.formatEach('stylish', process.stderr))
        .pipe(babel(BABEL_OPTIONS))
        .pipe(wrap('(function(){\n<%= contents %>\n})();'))
        .pipe(addStream.obj(prepareTemplates()))
        .pipe(concat('app.js'))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist'));
});

gulp.task('js-watch', ['js'], browserSync.reload);

gulp.task('serve', ['js'], function () {
    browserSync.init({
        server: {
            baseDir: './'
        }
    });

    gulp.watch(['src/**/*.*'], ['js']);
    gulp.watch(['index.html', 'dist/app.js']).on('change', browserSync.reload);
});
