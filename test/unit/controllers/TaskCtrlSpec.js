describe('TaskCtrl', function() {
    beforeEach(module('TodoApp'));

    var $controller;

    beforeEach(inject(function(_$controller_){
        $controller = _$controller_;
    }));

    it('constructor() fails without deps', function() {
        expect(function(){$controller('TaskCtrl', {})}).toThrow();
    });

    it('constructor() works with deps', function() {
        var loadedTask = [];
        var $state = {};
        var $scope = {$watch: function () {}};
        var TasksService = {};

        spyOn($scope, '$watch');

        var controller = $controller('TaskCtrl', {
            loadedTask: loadedTask,
            $state: $state,
            $scope: $scope,
            TasksService: TasksService
        });

        expect(controller.$state).toEqual($state);
        expect(controller.$scope).toEqual($scope);
        expect(controller.$scope).toEqual($scope);
        expect(controller.TasksService).toEqual(TasksService);
        expect($scope.$watch).toHaveBeenCalled();
    });

    it('save() works with deps', function() {
        var loadedTask = [];
        var $state = {};
        var $scope = {$watch: function () {}};
        var TasksService = {addOne: function () {return {then: function () {}}}};
        var event = {preventDefault: function () {}};
        var pendingTask = {};

        spyOn(TasksService, 'addOne').and.callThrough();
        spyOn(event, 'preventDefault');

        var controller = $controller('TaskCtrl', {
            loadedTask: loadedTask,
            $state: $state,
            $scope: $scope,
            TasksService: TasksService
        });

        controller.pendingTask = pendingTask;
        controller.save(event);

        expect(TasksService.addOne).toHaveBeenCalledWith(pendingTask);
        expect(event.preventDefault).toHaveBeenCalled();
    });
});
