describe('TasksListCtrl', function() {
    beforeEach(module('TodoApp'));

    var $controller;

    beforeEach(inject(function(_$controller_){
        $controller = _$controller_;
    }));

    it('constructor() fails without deps', function() {
        expect(function(){$controller('TasksListCtrl', {})}).toThrow();
    });

    it('constructor() works with deps', function() {
        var loadedTasks = [];
        var TasksService = {};
        var controller = $controller('TasksListCtrl', {
            loadedTasks: loadedTasks,
            TasksService: TasksService
        });

        expect(controller.orderOption).toEqual('name');
    });

    it ('change() invokes TasksService.addOne() method with param', function () {
        var task = {};
        var loadedTasks = [];
        var TasksService = {addOne: function (task) {}};

        spyOn(TasksService, 'addOne');

        var controller = $controller('TasksListCtrl', {
            loadedTasks: loadedTasks,
            TasksService: TasksService
        });

        controller.change(task);
        expect(TasksService.addOne).toHaveBeenCalledWith(task);

        controller.change(undefined);
        expect(TasksService.addOne).toHaveBeenCalledWith(undefined);
    });
});
